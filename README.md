# DBconnect

PDO database connection PHP class with PDOexception handler. It's made for small PHP projects with user interface: error messages are preformated with `<pre>[ERROR MESSAGE]</pre>` for HTML feedback.

## Test with :
- MySQL / MariaDB → *mysql* driver
- SQLite → *sqlite* driver

## SQLite
Think about doing some tricks :
- PRAGMA journal_mode=WAL
- PRAGMA busy_timeout=10000;

_To be more efficient on concurrency_
