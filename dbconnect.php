<?php

class Database{

  private $dbh;

  /*
  * $type → PDO driver to use
  * $location → SQL server or SQLite file
  * $database → Database to use
  * $user → User for database
  * $password → Password for database User
  */
  public function __construct($type,$location,$database=null,$user=null,$password=null) {
    $dbOptions = array(
      PDO::ATTR_PERSISTENT => true,
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
      PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
    );
    try{
      if($type=="sqlite") {
        $dsn = $type.':'.$location;
      }
      else {
        $dsn = $type.':host='.$location.';dbname='.$database;
        if($type=="mysql") $dsn = $dsn.';charset=utf8mb4';
      }
      $this->dbh = new PDO($dsn, $user, $password, $dbOptions);
    }
    catch(PDOException $e) {
      if($_SESSION['DEBUG'])
      die('<pre>Connection error : ' . $e->getMessage().'</pre>');
    }
  }

  /*
  * $sql: Request with ? for datas
  * $data: Datas to include in $sql request
  * $oneresult: TRUE for a unique result, FALSE for multiple results
  */
  function select($sql,$data=array(),$oneresult=false) {
    $query = $this->dbh->prepare($sql);
    try {
      $query->execute($data);

      if($_SESSION['DEBUG']) {
        echo '<pre>';$query->debugDumpParams(); echo '</pre>';
      }

      $oneresult ?
      $tmp=$query->fetch() :
      $tmp=$query->fetchAll();

      return $tmp;
    }
    catch (PDOException $e) {
      if($_SESSION['DEBUG'])
      die('<pre>SELECT ERROR : ' . $e->getMessage().'</pre>');
    }
  }

  /*
  * $sql: Request with ? for datas
  * $data: Datas to include in $sql request
  */
  function insert($sql,$data=array()) {
    $query = $this->dbh->prepare($sql);
    try {
      $query->execute($data);

      if($_SESSION['DEBUG']) {
        echo 'Inserted rows: '.$query->rowCount();
        echo '<pre>';$query->debugDumpParams(); echo '</pre>';
      }
    }
    catch (PDOException $e) {
      if($_SESSION['DEBUG'])
      die('<pre>INSERT ERROR : ' . $e->getMessage().'</pre>');
    }
  }

  /*
  * $sql: Request with ? for datas
  * $data: Datas to include in $sql request
  */
  function insertMultiple($sql,$data=array()) {
    $query = $this->dbh->prepare($sql);
    try {
      $this->dbh->beginTransaction();
      foreach ($data as $row) {
        $query->execute($row);
      }
      $this->dbh->commit();

      if($_SESSION['DEBUG']) {
        echo 'Inserted rows: '.$query->rowCount();
        echo '<pre>';$query->debugDumpParams(); echo '</pre>';
      }
    }
    catch (PDOException $e){
      $this->dbh->rollback();
      if($_SESSION['DEBUG'])
      die('<pre>INSERT (multiple) ERROR : ' . $e->getMessage().'</pre>');
    }
  }

  /*
  * $sql: Request with ? for datas
  * $data: Datas to include in $sql request
  */
  function update($sql,$data=array()) {
    $query = $this->dbh->prepare($sql);
    try {
      $query->execute($data);

      if($_SESSION['DEBUG']) {
        echo 'Uppdated rows: '.$query->rowCount();
        echo '<pre>';$query->debugDumpParams(); echo '</pre>';
      }
    }
    catch (PDOException $e) {
      if($_SESSION['DEBUG'])
      die('<pre>UPDATE error : ' . $e->getMessage().'</pre>');
    }
  }

  /*
  * $sql: Request with ? for datas
  * $data: Datas to include in $sql request
  */
  function delete($sql,$data=array()) {
    $query = $this->dbh->prepare($sql);
    try {
      $query->execute($data);

      $driver = $this->dbh->getAttribute(PDO::ATTR_DRIVER_NAME);

      if($driver=="sqlite") {
        $this->dbh->exec('VACUUM');
      }

      if($_SESSION['DEBUG']) {
        echo 'Deleted rows: '.$query->rowCount();
        echo '<pre>';$query->debugDumpParams(); echo '</pre>';
      }
    }
    catch (PDOException $e) {
      if($_SESSION['DEBUG'])
      die('<pre>DELETE error : ' . $e->getMessage().'</pre>');
    }
  }
}

?>
